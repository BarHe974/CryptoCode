# Projet Sécurité Informatique

## Informations générales :
Projet Chiffrement, Master 1 Informatique, Université de la Réunion.

## Démarrage :
- Méthode 1
    * Télécharger l'application (macos) : [ici](https://drive.google.com/file/d/1IO_Ocl5DwT1yjEudknUJmpMd5NV6N1tq/view?usp=sharing)
    * Lors de l'exécution il se pourrait que MAC OS vous affiche ce message :

    > Impossible d’ouvrir « cryptocode » car le développeur ne peut pas être vérifié.

    * Cliquez alors sur `Annuler` puis dans `Sécurité et Confidentialité` cliquez sur `Ouvrir quand même`.
    * Ce problème arrive car il faut une signature développeur et un ID développeur mac.

- Méthode 2
    * Installer nodejs sur votre ordinateur
    * Dans le dossier du projet, executer les commandes suivantes dans un terminal externe :
        1. Sur Windows
            - `npm install`
            - `npm start`
        2. Sur Mac
            - `sudo npm install`
            - `sudo npm start`
            - Si cela ne fonctionne pas exécuter : `sudo npm install -g electron --unsafe-perm=true --allow-root` puis `npm start`

- Méthode 3
    * Executer le fichier index.html dans votre navigateur (cette méthode ne prendra pas en compte certaines fonctions de l'application tels que les alert en cas de mauvaises saisies)

## Auteurs :
Hervé Barret et Emilie Lebon, Master 1 Informatique, Université de la réunion.

## API :
Node Electron

## Fonctionnalités :
Chiffrements disponibles : Atbash, Vigenère, Hill, Transposition Rectangulaire, Delastelle.

## Spécificités des chiffrements : 
- Tous les chiffrements prennent en paramètres des caractères :
    * Vigenère, Hill, Transposition Rectangulaire : du caractère espace (code ASCII décimal = 32) au caractère tilde (~, code ASCII décimal = 126).
    * Atbash : abcdefghijklmnopqrstuvwxyz (uniquement minuscule)
    * Delastelle : les 26 lettres de l'alphabet + les 10 chiffres
- Les espaces sont automatiquement supprimés dans le chiffre de Delastelle.
- Des messages d'erreur sont générés si les critères ne sont pas respectés. Exemple : champs non remplis.
